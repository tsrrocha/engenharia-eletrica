# Engenharia Elétrica

## Disciplinas

### Período 1 - 2021.1

- [Probabilidade e Estatística](https://gitlab.com/tsrrocha/engenharia-eletrica/-/tree/master/disciplinas/probabilidade%20e%20estatistica)

- [Algoritmos e Programação](https://gitlab.com/tsrrocha/engenharia-eletrica/-/tree/master/disciplinas/algoritimos%20e%20programacao)

- Engenharia e Inovação

