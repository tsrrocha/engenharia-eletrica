# Atividade Avaliativa A2

[[_TOC_]]


Esta atividade deve ser entregue até às 17:00h do dia 27/03/2021.

### Atividade:

O procedimento de perda rápida de "peso" é comum entre os atletas dos esportes de combate.  
Para  participar  de  um  torneio,  quatro  atletas  da  categoria  até  66kg,  PesoPena,  
foram  submetidos  a  dietas  balanceadas  e  atividades  físicas.  

Realizaram  três "pesagens"  antes  do  início  do  torneio.  
Pelo  regulamento  do  torneio, a primeira luta deverá ocorrer entre o atleta menos regular 
e o mais regularquanto aos "pesos". 

As informações com base nas pesagens dos atletas estão no quadro.

|Atleta|1ª Pesagem (Kg)|2ª Pesagem (Kg)|3ª Pesagem (Kg)|
|:----:|:-------------:|:-------------:|:-------------:|
|  I   |78|72|66|
|  II  |83|65|65|
| III  |75|70|65|
| IV   |80|77|62|

Após as três "pesagens", os organizadores do torneio informaram aos atletas quais deles se enfrentariam na primeira luta.

Para resolver o problema, um estatístico orientou usar o conceito de desvio-padrão para a resolução do problema. 

Portanto, aprimeira luta foi entre os atletas(justifique sua resposta com os cálculos):

a) I e III.

b) I e IV.

c) II e III.

d) II e IV.

e) III e IV.


-------



### Resolução:

Para resolver essa questão precisaremos encontrar qual é o atleta mais regular e também o menos regular. 

Isso pode ser feito através da análise do **desvio-padrão** de forma que o atleta que

obtiver o menor desvio-padrão 
será o **mais regular** e o que obtiver o maior desvio-padrão será o atleta **menos regular**.

#### Cálculo do Desvio-Padrão

##### Atleta I:

|$`x`$|$`x^2`$|
|:---:|:-----:|
|78|6084|
|72|5184|
|66|4356|

> Somatórios:

```math
\sum{ x_I } = (78 + 72 + 66) = 216
```

```math
\sum{ x_I^2 } = (6084 + 5184 + 4356) = 15624
```

> Desvio-padrão:

```math
\sigma_I = \sqrt{ \frac{ \sum{x_I^2} - \frac{( \sum{x_I} )^2} N } N } = \sqrt{ \frac{ 15624 - \frac{( 216 )^2} 3 } 3 }

= \sqrt{ \frac{ 15624 - \frac{46656} 3 } 3 } = \sqrt{ \frac{ 15624 - 15552 } 3 } = \sqrt{ \frac{ 72 } 3 }

= \sqrt{ 24 } 

= 4,8989

```


-------


##### Atleta II:

|$`x`$|$`x^2`$|
|:---:|:-----:|
|83|6889|
|65|4225|
|65|4225|

> Somatórios:

```math
\sum{ x_I } = (83 + 65 + 65) = 213
```

```math
\sum{ x_I^2 } = (6889 + 4225 + 4225) = 15339
```

> Desvio-padrão:

```math
\sigma_I = \sqrt{ \frac{ \sum{x_I^2} - \frac{( \sum{x_I} )^2} N } N } = \sqrt{ \frac{ 15339 - \frac{( 213 )^2} 3 } 3 }

= \sqrt{ \frac{ 15339 - \frac{45369} 3 } 3 } = \sqrt{ \frac{ 15339 - 15123 } 3 } = \sqrt{ \frac{ 216 } 3 }

= \sqrt{ 72 } 

= 8,4852

```


------


##### Atleta III:

|$`x`$|$`x^2`$|
|:---:|:-----:|
|75|5625|
|70|4900|
|65|4225|

> Somatórios:

```math
\sum{ x_I } = (75 + 70 + 65) = 210
```

```math
\sum{ x_I^2 } = (5625 + 4900 + 4225) = 14750
```

> Desvio-padrão:

```math
\sigma_I = \sqrt{ \frac{ \sum{x_I^2} - \frac{( \sum{x_I} )^2} N } N } = \sqrt{ \frac{ 14750 - \frac{( 210 )^2} 3 } 3 }

= \sqrt{ \frac{ 14750 - \frac{44100} 3 } 3 } = \sqrt{ \frac{ 14750 - 14700 } 3 } = \sqrt{ \frac{ 50 } 3 }

= \sqrt{ 16,6667 } 

= 4,0824

```


-------



##### Atleta IV:

|$`x`$|$`x^2`$|
|:---:|:-----:|
|80|6400|
|77|5929|
|62|3844|

> Somatórios:

```math
\sum{ x_I } = (80 + 77 + 62) = 219
```

```math
\sum{ x_I^2 } = (6400 + 5929 + 3844) = 16173
```

> Desvio-padrão:

```math
\sigma_I = \sqrt{ \frac{ \sum{x_I^2} - \frac{( \sum{x_I} )^2} N } N } = \sqrt{ \frac{ 16173 - \frac{( 219 )^2} 3 } 3 }

= \sqrt{ \frac{ 16173 - \frac{47961} 3 } 3 } = \sqrt{ \frac{ 16173 - 15987 } 3 } = \sqrt{ \frac{ 186 } 3 }

= \sqrt{ 62 } 

= 7,8740

```


-------


#### Resultado

> Desvio-padrão:

|Atleta I|Atleta II|Atleta III|Atleta IV|
|:--:|:--:|:--:|:--:|
|4,8989|8,4852|4,0824|7,8740|

O atleta mais regular foi o **Atleta III** e o atleta menos regular foi o **Atleta II**.

A resposta correta é a **letra C**.


------
