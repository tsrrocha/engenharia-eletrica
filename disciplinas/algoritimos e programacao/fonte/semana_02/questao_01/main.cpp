/**
 *      Atividade da Semana 02 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Este ...
 *
 */
 #include <iostream>
 using namespace std;

void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Atividade da Semana 02 - Questão 01" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Cálculo do IMC                                           ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    float peso = 0, altura = 0, imc = 0;
    //
    printCabecalho();

    // Coleta o peso
    cout << "Informe o peso da pessoa, em quilogramas (Kg):" << endl;
    cin >> peso;

    // Coleta o peso
    cout << "Informe a altura da pessoa, em centímetros (Cm):" << endl;
    cin >> altura;
    altura = (float)(altura / 100);
    
    // Cálcula o IMC
    imc = (float) (peso / ((altura * altura)));

    // Exibe o resultado
    cout << "Resultado: " << endl;
    if (imc < 18.5f) {
        cout << "IMC=" << imc << ", Situação: Peso baixo." << endl;
    } else if ((imc >= 18.5f) && (imc <= 24.9f)){
        cout << "IMC=" << imc << ", Situação: Peso normal." << endl;
    } else if ((imc >= 25.0f) && (imc <= 29.9f)){
        cout << "IMC=" << imc << ", Situação: Sobrepeso" << endl;
    } else if ((imc >= 30.0f) && (imc <= 34.9f)){
        cout << "IMC=" << imc << ", Situação: Obesidade (Grau I)" << endl;
    } else if ((imc >= 35.0f) && (imc <= 39.9f)){
        cout << "IMC=" << imc << ", Situação: Obesidade Severa (Grau II)" << endl;
    } else if (imc >= 40.0f){
        cout << "IMC=" << imc << ", Situação: Obesidade Mórbida (Grau III)" << endl;
    }

};