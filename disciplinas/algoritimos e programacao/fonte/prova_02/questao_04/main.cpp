/**
 *      Prova 2 - Questão 04
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 04" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    float matriz[3][3];
    float vetor[3];
    int row = 0, col = 0;
    float number = 0.0f;

    //
    printCabecalho();

    //
    cout << "Você irá informar os valores para preencher uma matriz 3 x 3." << endl;

    for (row = 0; row < 3; row++) {
        cout << "Preenchimento da linha " << (row + 1) << ". " << endl;
        for (col = 0; col < 3; col++) {
            cout << "Por favor, informe um valor para o índice: (linha,coluna) [" << (row + 1) << "," << (col + 1) << "]: " <<  endl;
            cin >> number;

            // 
            vetor[row] += number;
            matriz[row][col] = number;
        }
    }

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Somatório da linha 1: " << vetor[0] << endl;
    cout << "Somatório da linha 2: " << vetor[1] << endl;
    cout << "Somatório da linha 3: " << vetor[2] << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
};
