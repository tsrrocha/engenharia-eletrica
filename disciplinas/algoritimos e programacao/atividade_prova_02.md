


## Questão 1

Escreva um programa que calcule o desvio padrão de um número n de elementos de um vetor A.

**Entradas**: vetor com n elementos;
**Saídas**: Desvio padrão.


> **Dica**: utilize a equação $`desvio padrão = \sqrt{ (\frac{1} {(n - 1)}) * \sum\limits_{i=1}^{n}( A[i] - med )^2 } `$, onde $`med`$ é a média dos valores do vetor.


### Resolução

#### Fluxograma

![Fluxograma do algoritmo da questão 1.](images/fluxograma_prova2_questao1.png)


#### Código-fonte

```cpp
/**
 *      Prova 2 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 #include <math.h>
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 01                                               ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    float numberElements = 0.0f;
    int numberElementsInt = 0;
    int row = 0;
    float value = 0.0f, somaGeral = 0.0f, mediaGeral = 0.0f, somaMed = 0.0f;
    float desvioPadrao = 0.0f;

    //, sDiagMain = 0.0f, sDiagDown = 0.0f, sDiagUp = 0.0f;

    //
    printCabecalho();

    do {
        cout << "Informe a quantidade de linhas que a matriz quadrada terá:" << endl;
        cin >> numberElements;
        numberElementsInt = (int) numberElements;
        if ( (numberElementsInt < 0) || ( (numberElements - numberElementsInt) != 0) ) {
            cout << "O número informado é inválido. Tem de ser um número inteiro maior que 0(zero)." << endl << endl << endl;
        }
    } while ( (numberElementsInt < 0) || ( (numberElements - numberElementsInt) != 0) );
    float vetor[numberElementsInt];

    //
    cout << "Você irá informar os valores para preencher um vetor "<< numberElementsInt <<"."<< endl;

    for (row = 0; row < numberElementsInt; row++) {
        cout << "Informe um número para a posição " << (row + 1) << ". " << endl;
        cin >> value;
        //
        vetor[row] = value;
        somaGeral += value;
    }

    // Calcula a média
    mediaGeral = (float) (somaGeral / (float) numberElementsInt);

    for (row = 0; row < numberElementsInt; row++) {
        //
        vetor[row] = pow((vetor[row] - mediaGeral), 2) ;
        somaMed += vetor[row];
    }

    // Raiz
    desvioPadrao = sqrt( (float) ( somaMed / (float) numberElementsInt ) );

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Desvio padrão é: " << desvioPadrao << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
};
```

------

<br/><br/><br/><br/>


## Questão 2

Desenvolva um programa que escreva um vetor de 10 elementos inteiros e positivos e que apresenta, separadamente, a soma dos valores pares e impares.

**Entrada**: A [ 10 ].
**Saídas**: soma_par e soma_impar.

> **Dicas**: Utilize o comando %, resto = a % 2.


### Resolução

#### Fluxograma

![Fluxograma do algoritmo da questão 2.](images/fluxograma_prova2_questao2.png)


#### Código-fonte

```cpp
/**
 *      Prova 2 - Questão 02
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 02" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    int soma_par = 0, soma_impar = 0;
    int n = 0, numberInt = 0;
    float number = 0.0;
    int arrNumeros[10];
    //
    printCabecalho();

    for (n = 0; n < 10; n++) {
        cout << "Informe um número inteiro para a posição: " << (n + 1) << endl;
        do {
            cin >> number;
            numberInt = (int) number;
            if ( (numberInt < 0) || ((number - numberInt) != 0) ) {
                cout << "O número informado é inadequado. " << endl;
                cout << "Por favor, informe um número inteiro e maior que 0 para a posição " << (n + 1) << ": " << endl;
            }
        } while ( (numberInt < 0) || ((number - numberInt) != 0) );

        // verifica se o número é par
        if ((numberInt % 2) == 0) {
            soma_par += numberInt;
        } else {
            soma_impar += numberInt;
        }
    }

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Somatório de números pares é: " << soma_par << endl;
    cout << "Somatório de números ímpares é: " << soma_impar << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;

};
```

------

<br/><br/><br/><br/>


## Questão 3

Produza um algoritmo que escreva uma matriz quadrada $`n x n`$ e que sejam  feitas as somas dos elementos na 
diagonal principal (sDiagonal), soma dos elementos abaixo da diagonal principal (sAbaixoDiagonal) e acima da diagonal principal (sAcimaDiagonal).


**Entrada**: A[n][n].
**Saída**: sDiagonal, sAbaixoDiagonal e sAcimaDiagonal.


### Resolução

#### Fluxograma

![Fluxograma do algoritmo da questão 3.](images/fluxograma_prova2_questao3.png)



#### Código-fonte

```cpp
/**
 *      Prova 2 - Questão 03
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 03                                               ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    int row = 0, col = 0;
    float value = 0.0f, sDiagMain = 0.0f, sDiagDown = 0.0f, sDiagUp = 0.0f;
    float numberRows = 0.0f;
    int numberInt = 0;

    //
    printCabecalho();

    do {
        cout << "Informe a quantidade de linhas que a matriz quadrada terá:" << endl;
        cin >> numberRows;
        numberInt = (int) numberRows;
        if ( (numberInt < 0) || ( (numberRows - numberInt) != 0) ) {
            cout << "O número informado é inválido. Tem de ser um número inteiro maior que 0(zero)." << endl << endl << endl;
        }
    } while ( (numberInt < 0) || ( (numberRows - numberInt) != 0) );
    float matriz[numberInt][numberInt];

    //
    cout << "Você irá informar os valores para preencher uma matriz "<< numberInt <<" x "<< numberInt <<"."<< endl;

    for (row = 0; row < numberInt; row++) {
        cout << "Preenchimento da linha " << (row + 1) << ". " << endl;

        for (col = 0; col < numberInt; col++) {
            cout << "Por favor, informe um valor para o índice: (linha,coluna) [" << (row + 1) << "," << (col + 1) << "]: " <<  endl;
            cin >> value;
            // 
            matriz[row][col] = value;
            //
            if (row == col) {
                // Diagonal Principal
                sDiagMain += value;
            } else if (row > col) {
                // Abaixo da Diagonal
                sDiagDown += value;
            } else {
                // Acima da Diagonal
                sDiagUp += value;
            }
        }
    }

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Somatório da Diagonal Principal: " << sDiagMain << endl;
    cout << "Somatório dos elementos abaixo da Diagonal: " << sDiagDown << endl;
    cout << "Somatório dos elementos acima da Diagonal: " << sDiagUp << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
};

```

------

<br/><br/><br/><br/>


## Questão 4

Escreva uma matriz 3x3 e escreva um vetor de 3 elementos que apresente a soma dos elementos de cada linha da matriz.

Por exemplo:
```math
\left(\begin{array}{cc}
1 & 8 & -9\\
-2 & 10 & 3\\
-7 & 15 & -2
\end{array}\right)
=
\left(\begin{array}{cc}
0 & 11 & 6
\end{array}\right)
```



**Entrada**: A[3][3].
**Saída**: vet[3].


### Resolução


#### Fluxograma

![Fluxograma do algoritmo da questão 4.](images/fluxograma_prova2_questao4.png)


#### Código-fonte

```cpp
/**
 *      Prova 2 - Questão 04
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 04" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    float matriz[3][3];
    float vetor[3];
    int row = 0, col = 0;
    float number = 0.0f;

    //
    printCabecalho();

    //
    cout << "Você irá informar os valores para preencher uma matriz 3 x 3." << endl;

    for (row = 0; row < 3; row++) {
        cout << "Preenchimento da linha " << (row + 1) << ". " << endl;
        for (col = 0; col < 3; col++) {
            cout << "Por favor, informe um valor para o índice: (linha,coluna) [" << (row + 1) << "," << (col + 1) << "]: " <<  endl;
            cin >> number;

            // 
            vetor[row] += number;
            matriz[row][col] = number;
        }
    }

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Somatório da linha 1: " << vetor[0] << endl;
    cout << "Somatório da linha 2: " << vetor[1] << endl;
    cout << "Somatório da linha 3: " << vetor[2] << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
};
```

------

<br/><br/><br/><br/>


## Questão 5 - Bônus

Escreva 2 vetores de elementos n e determine se eles são *lineramente dependentes (LD)* ou *lineramente independentes (LI)*. 


### Resolução



#### Fluxograma



#### Código-fonte

```cpp


```

------

