---
title: "Engenharia e Inovação - Prova 01"
author: "Tiago Sousa Rocha"
date: " 14 de abril de 2021"
header: "Header TIAGO "
footer: "Tiago Sousa Rocha, Matricula: 202111479"
geometry: margin=2cm
output: pdf_document
---





**Diante do que conversamos durante nossas aulas e de acordo com suas experiências, 
escreva um texto de no máximo duas laudas que aborde os seguintes temas:**



## O que é Engenharia para você?

A **Engenharia** é para mim a aplicação do conhecimento científico ou empírico, que envolve várias áreas como a química, física, matemática entre outras, na construção, manutenção ou melhoria de um produto ou solução resolvendo algum tipo de dificuldade ou problema pela qual passa a humanidade. 

A Engenharia trás diversos benefícios para a humanidade através da resolução dos problemas, entre eles está: o econômico, que se estende desde o ganho de produtividade, como também o de tornar possível a execução de determinado proceso, como por exemplo: mensurar a temperatura de um forno para a produção de peças em cerâmica. Não fosse a Engenharia seria muito arriscado um ser humano realizar essa aferição dentro desse tipo de forno.



## Por que você quer ser engenheiro(a)?

Devido a minha formação técnica ser relacionada a ***Engenharia Elétrica***, pois sou técnico em eletrônica com habilitação em indústria pela Escola Técnica Redentorista e sou graduado em Análise e Desenvolvimento de Sistemas, tenho grande interesse em unir o *Hardware* com o *Software* para criar soluções para o mercado. Aliado a isso, trabalho na Zênite Tecnologia uma empresa que trabalha forte a *inovação* e que me permite atuar nessa área proporcionando prazer e satisfação.

Minha pretensão a longo prazo é de seguir a carreira acadêmica, fazendo mestrado e doutorado na área da Engenharia me tornando um professor
me espelhando nos nobres professores que tive a oportunidade de conhecer.



## Quais os maiores desafios que você espera enfrentar no mercado de trabalho?

Entendo que um dos desafios seja trabalhar com as tecnologias de inteligência artificial (AI) e machine learning, porém, acredito também que isso se dê ao pouco conhecimento que tenho sobre o assunto no momento, por isso considero como um desafio. 

Dentre os usos da tecnologia de Inteligência Artificial estão as soluções que envolvem a análise de dados em tempo real ou quase isso, como por exemplo: utilizar inteligência artificial para analisar uma chamada telefônica e identificar palavras inadequadas.

Essa área muito promissora se torna um desafio para os Engenheiros Eletricistas bem como para os Cientistas de Dados. Também nessa linha está o uso das energias renováveis para um novo mundo ecologicamente correto.

\newpage


## Como você encara a quarta revolução industrial e quais os desafios e facilidades que ela trará para você?

Encaro a quarta revolução industrial com muito bons olhos, possibilitando qua as empresas possam mensurar e acompanhar graficamente 
em tempo real e de qualquer lugar o processo produtivo. Por exemplo: Um amigo engenheiro em São Paulo criou uma rede Mesh para receber conexões de plaquinhas (ESP32 com FreeRTOS) para monitorar bancos de baterias. Essas informações são enviadas para uma plataforma onde é feito o monitoramento gráfico dessas baterias e o melhor: as regras e tomadas de decisão automaticamente de acordo com os dados,
 possibilitando a notificação em tempo real de alguma anomalia em alguma bateria.

Os desafios que terei será de aprender como funciona essas novas tecnologias, protocolos e plataformas, para criação de soluções que 
auxiliem o mercado a aumentar produtividade, minimizar as perdas e que isso possa ser viável para o mercado.


