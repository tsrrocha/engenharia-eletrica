
# **Medidas Dispersão**

[[_TOC_]]


## Desvio Padrão


O desvio-padrão é obtido extraindo-se a raiz quadrada positiva da variância. 
O desvio-padrão pode ser calculado para uma *população*, representado pela letra $`\sigma`$ (sigma), ou para uma *amostra*, representado pela letra $`s`$. 
Seu cálculo é feito da seguinte forma:

> Para uma população: $`\sigma = \sqrt{ \frac{ \sum{ (x - \mu)^2 } } N }`$

Onde, $`\mu`$ é a média aritmética do conjunto de dados e $`x`$ é o valor individual.

> Para uma amostra: $`s = \sqrt{ \frac{ \sum{ (x - \bar x)^2 } } {n - 1} }`$

Onde, $`\bar x`$ é a média aritmética do conjunto de dados e $`x`$ é o valor individual.


### Desvio Padrão - Atalho

> Para população $` \sigma = \sqrt{ \frac{ \sum{x^2} - \frac{( \sum{x} )^2} N } N } `$


> Para amostra $` s = \sqrt{ \frac{ \sum{x^2} - \frac{(\sum{x})^2} {n} } {n - 1} }`$


### Exemplo :blue_book:



--------





