# Algoritmos e Programação

[[_TOC_]]


## Fluxo das aulas

```mermaid
graph LR
  S01["Semana 01"]-->A01_1("Aula 1 - Introdução aos conceitos de algoritmo - 15/03/2021")
  S01-->A02_1("Aula 2 - Tipos de variáveis e introdução a C++ - 16/03/2021")
  S02["Semana 02"]-->A03_1("Aula 3 - Desenvolvimento de algoritmos por meio de fluxogramas - 22/03/2021")
  S02-->A04_1("Aula 4 - Implementação dos algoritmos no DevC++ - 23/03/2021")
  S03["Semana 03"]
  S04["Semana 04"]
```



## Atividades

### Semana 02

[Página para a atividade](https://gitlab.com/tsrrocha/engenharia-eletrica/-/blob/master/disciplinas/algoritimos%20e%20programacao/atividade_semana_02.md)

### Prova 01

[Página para a Prova](https://gitlab.com/tsrrocha/engenharia-eletrica/-/blob/master/disciplinas/algoritimos%20e%20programacao/atividade_prova_01.md)


### Prova 02

[Página para a Prova](https://gitlab.com/tsrrocha/engenharia-eletrica/-/blob/master/disciplinas/algoritimos%20e%20programacao/atividade_prova_02.md)