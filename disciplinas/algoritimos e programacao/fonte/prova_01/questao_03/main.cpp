/**
 *      Prova 1 - Questão 03
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que recebe 1 número e calcula o seu fatorial
 *
 */
 #include <iostream>
 using namespace std;

void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Prova 01 - Questão 03" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Calcular o fatorial de um número                         ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    int number = 0, somatorio = 1;

    //
    printCabecalho();

    do {
        if (number != 0) {
            cout << endl;
            cout << endl;
            cout << "O número deve estar entre 1 e 12. " << endl;
        }
        cout << "Informe um número inteiro entre 1 e 12: " << endl;
        cin >> number;
    } while ( !((number > 0) && (number <= 12)) );

    for (int idx = number; idx > 0; idx--) {
        //cout << "<DEBUG> ["<<idx<<"]" << somatorio << " * " << idx << endl;
        somatorio *= idx;
    }


    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "========================================================================" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << "Fatorial de " << number << ", é: " << somatorio << endl;

};
