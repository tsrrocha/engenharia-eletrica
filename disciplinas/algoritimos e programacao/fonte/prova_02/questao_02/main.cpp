/**
 *      Prova 2 - Questão 02
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 02" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    int soma_par = 0, soma_impar = 0;
    int n = 0, numberInt = 0;
    float number = 0.0;
    int arrNumeros[10];
    //
    printCabecalho();

    for (n = 0; n < 10; n++) {
        cout << "Informe um número inteiro para a posição: " << (n + 1) << endl;
        do {
            cin >> number;
            numberInt = (int) number;
            if ( (numberInt < 0) || ((number - numberInt) != 0) ) {
                cout << "O número informado é inadequado. " << endl;
                cout << "Por favor, informe um número inteiro e maior que 0 para a posição " << (n + 1) << ": " << endl;
            }
        } while ( (numberInt < 0) || ((number - numberInt) != 0) );

        // verifica se o número é par
        if ((numberInt % 2) == 0) {
            soma_par += numberInt;
        } else {
            soma_impar += numberInt;
        }
    }

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Somatório de números pares é: " << soma_par << endl;
    cout << "Somatório de números ímpares é: " << soma_impar << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;

};
