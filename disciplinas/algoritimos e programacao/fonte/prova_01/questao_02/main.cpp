/**
 *      Prova 1 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que calcule as raízes e ponto máximo de uma equação de segundo grau.
 *
 */
 #include <iostream>
 using namespace std;
 #include <math.h>

void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 01 - Questão 02" << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Calcular medidas de uma circunferência/esfera através do raio    ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    float raio = 0.0f, diametro = 0.0f, perimetro = 0.0f, areaCircunf = 0.0f;
    float volEsfera = 0.0f, areaCascaEsfera = 0.0f;

    //
    printCabecalho();

    do {
        cout << "Informe o valor do Raio de uma circunferência (Cm): " << endl;
        cin >> raio;
        if (raio <= 0.0f)
            cout << "É necessário que o Raio seja maior que 0." << endl;
    } while ( raio <= 0.0f );

    // Calcula Diâmetro
    diametro = 2 * raio;

    // Calcula Perímetro
    perimetro = 2 * M_PI * raio;

    // Calcula a Área da Circunferência
    areaCircunf = M_PI * pow(raio, 2);

    // Calcula Volume de uma Esfera
    volEsfera = (float)(4 * M_PI * pow(raio, 3)) / (float)(3);

    // Calcula a Área da Casca de uma Esfera
    areaCascaEsfera = 4 * M_PI * pow(raio, 2);

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "===================================================================================" << endl;
    cout << "a) Diâmetro = " << diametro << " Cm "<< endl;
    cout << "b) Perímetro da circunferência = " << perimetro << " Cm "<< endl;
    cout << "c) Área da circunferência = " << areaCircunf << " Cm quadrados"<< endl;
    cout << "d) Volume de uma esfera = " << volEsfera  << " Cm cúbicos"<< endl;
    cout << "e) Área da casca de uma esfera = " << areaCascaEsfera << " Cm quadrados"<< endl;
    cout << "-----------------------------------------------------------------------------------" << endl;

};
