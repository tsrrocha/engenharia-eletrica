
## **Medidas Descritivas Numéricas:**

[[_TOC_]]


### *Média Aritmética*

A média aritmética representa a medida de tendência central mais frequentemente utilizada.

- Média aritmética para os dados da população: ( $` \sum x `$ ) soma de todos os valores dividido pela ( $` N `$ ) quantidade de valores.

```math
\mu = \frac{\sum x} N
```

- Média aritmética para os dados da amostra:
```math
\bar x = \frac{\sum x} n 
```

----

### *Mediana*

A mediana representa o valor correspondente ao termo posicionado no meio de um conjunto de dados que tenha sido ordenado em ordem crescente.

O cálculo da mediana consiste em duas etapas:
- Classificar o conjunto de dados em ordem crescente;
- Encontrar o termo posicionado no meio. O valor desse termo corresponde à mediana.

 **Observação:** Caso o número de observações em um conjunto de dados seja *ímpar*, a mediana é fornecida com base no valor do termo posicionado no meio dos dados classificados em ordem crescente. Porém, caso o número de observações seja *par*, a mediana é fornecida com base na média entre os valores correspondentes aos dois termos do meio.


 > Números:

|21,6|21,7|22,9|25,2|26,5|28,0|28,2|32,6|32,9|70,1|76,1|84,5|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
||||||:arrow_up:|:arrow_up:||||||

 > Mediana:

A mediana será calculada entre os números ***28.0*** e ***28.2***, dessa forma:
```math
{Mediana} = \frac{ 28.0 + 28.2} 2 = \frac{56.2} 2 = 28.1 
```


----



### *Moda*

A *moda* corresponde ao valor que ocorre com maior frequência em um conjunto de dados.

|77|82|74|81|79|84|74|78|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| | |:white_check_mark:|| | |:white_check_mark:| |

No conjunto de dados acima, o valor que tem maior frequência é o ***74***, por tanto:
```math
{Moda} = 74
```

----

