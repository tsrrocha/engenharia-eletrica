# Atividades da Semana 2

[[_TOC_]]


## Questão 1

Construa um algoritmo (fluxograma) e escreva um código (c++) que estabeleça o IMC de um indivíduo (Código 04 - S01)
e estabeleça onde o indivíduo está em relação a classificação apresentada na seguinte tabela:

|IMC|Classificação|
|:--:|:--:|
| < 18,5|Peso baixo|
|18,5 - 24,9|Peso normal|
|25,0 - 29,9|Sobrepeso|
|30,0 - 34,9|Obesidade (Grau I)|
|35,0 - 39,9|Obesidade Severa (Grau II)|
| >= 40,0|Obesidade Mórbida (Grau III)|


**Entradas**: Peso e altura.
**Saída**: IMC e posição na tabela de classificação.

> Dica: Utilize o comando ***if - else*** para determinar os intervalos onde o IMC está posicionado, para tanto, 
lembre-se de estabelecer lógica combinatória ***"e"*** ($`&&`$) e ***"ou"*** ($`||`$) entre os valores quando necessário.

<br/>
<br/>
<br/>

### Resolução

<br/>
<br/>

#### Fluxograma

![Fluxograma para cálculo do IMC.](images/fluxograma_imc.png)

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


#### Código-fonte

Arquivo ***main.cpp*** :


```cpp
/**
 *      Atividade da Semana 02 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Este ...
 *
 */
 #include <iostream>
 using namespace std;

void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Atividade da Semana 02 - Questão 01" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Cálculo do IMC                                           ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    float peso = 0, altura = 0, imc = 0;
    //
    printCabecalho();

    // Coleta o peso
    cout << "Informe o peso da pessoa, em quilogramas (Kg):" << endl;
    cin >> peso;

    // Coleta o peso
    cout << "Informe a altura da pessoa, em centímetros (Cm):" << endl;
    cin >> altura;
    altura = (float)(altura / 100);
    
    // Cálcula o IMC
    imc = (float) (peso / ((altura * altura)));

    // Exibe o resultado
    cout << "Resultado: " << endl;
    if (imc < 18.5f) {
        cout << "IMC=" << imc << ", Situação: Peso baixo." << endl;
    } else if ((imc >= 18.5f) && (imc <= 24.9f)){
        cout << "IMC=" << imc << ", Situação: Peso normal." << endl;
    } else if ((imc >= 25.0f) && (imc <= 29.9f)){
        cout << "IMC=" << imc << ", Situação: Sobrepeso" << endl;
    } else if ((imc >= 30.0f) && (imc <= 34.9f)){
        cout << "IMC=" << imc << ", Situação: Obesidade (Grau I)" << endl;
    } else if ((imc >= 35.0f) && (imc <= 39.9f)){
        cout << "IMC=" << imc << ", Situação: Obesidade Severa (Grau II)" << endl;
    } else if (imc >= 40.0f){
        cout << "IMC=" << imc << ", Situação: Obesidade Mórbida (Grau III)" << endl;
    }

};

```

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>



## Questão 2 (Desafio)

Utilizando a função *"%"*, ques estabelece o resto de uma divisão entre dois números, 
um algoritmo (fluxograma) e escreva um código (C++) que determine se um número é ímpar ou par.

**Entrada**: número inteiro para ser testado.
**Saída**: Número e conclusão se o mesmo é ímpar ou par.

<br/>


### Resolução

<br/>

#### Fluxograma

![Fluxograma para cálculo de número Par ou Ímpar](images/fluxograma_par.png)


<br/>
<br/>
<br/>
<br/>
<br/>


#### Código Fonte

```cpp
/**
 *      Atividade da Semana 02 - Questão 02
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Este programa lê um número inteiro e verifica se o mesmo é Par ou Ímpar.
 *
 */
 #include <iostream>
 using namespace std;

void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Atividade da Semana 02 - Questão 02" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Determinar se um número inteiro é Par ou Ímpar           ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    int numero = 0, resto = 0;
    //
    printCabecalho();

    // Coleta do número
    cout << "Informe um número inteiro:" << endl;
    cin >> numero;

    // Determina se é par ou ímpar
    resto = numero % 2;

    // Exibe o resultado
    cout << "Resultado: " << endl;

    if (resto == 0) {
        cout << "O ńumero: " << numero << ", é par." << endl;
    } else {
        cout << "O ńumero: " << numero << ", é ímpar." << endl;
    }

};
```


------




