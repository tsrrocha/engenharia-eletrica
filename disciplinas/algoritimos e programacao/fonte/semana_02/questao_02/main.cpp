/**
 *      Atividade da Semana 02 - Questão 02
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Este programa lê um número inteiro e verifica se o mesmo é Par ou Ímpar.
 *
 */
 #include <iostream>
 using namespace std;

void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Atividade da Semana 02 - Questão 02" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Determinar se um número inteiro é Par ou Ímpar           ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    int numero = 0, resto = 0;
    //
    printCabecalho();

    // Coleta do número
    cout << "Informe um número inteiro:" << endl;
    cin >> numero;

    // Determina se é par ou ímpar
    resto = numero % 2;

    // Exibe o resultado
    cout << "Resultado: " << endl;

    if (resto == 0) {
        cout << "O ńumero: " << numero << ", é par." << endl;
    } else {
        cout << "O ńumero: " << numero << ", é ímpar." << endl;
    }

};