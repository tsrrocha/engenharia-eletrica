/**
 *      Prova 2 - Questão 03
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 03                                               ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    int row = 0, col = 0;
    float value = 0.0f, sDiagMain = 0.0f, sDiagDown = 0.0f, sDiagUp = 0.0f;
    float numberRows = 0.0f;
    int numberInt = 0;

    //
    printCabecalho();

    do {
        cout << "Informe a quantidade de linhas que a matriz quadrada terá:" << endl;
        cin >> numberRows;
        numberInt = (int) numberRows;
        if ( (numberInt < 0) || ( (numberRows - numberInt) != 0) ) {
            cout << "O número informado é inválido. Tem de ser um número inteiro maior que 0(zero)." << endl << endl << endl;
        }
    } while ( (numberInt < 0) || ( (numberRows - numberInt) != 0) );
    float matriz[numberInt][numberInt];

    //
    cout << "Você irá informar os valores para preencher uma matriz "<< numberInt <<" x "<< numberInt <<"."<< endl;

    for (row = 0; row < numberInt; row++) {
        cout << "Preenchimento da linha " << (row + 1) << ". " << endl;

        for (col = 0; col < numberInt; col++) {
            cout << "Por favor, informe um valor para o índice: (linha,coluna) [" << (row + 1) << "," << (col + 1) << "]: " <<  endl;
            cin >> value;
            // 
            matriz[row][col] = value;
            //
            if (row == col) {
                // Diagonal Principal
                sDiagMain += value;
            } else if (row > col) {
                // Abaixo da Diagonal
                sDiagDown += value;
            } else {
                // Acima da Diagonal
                sDiagUp += value;
            }
        }
    }

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Somatório da Diagonal Principal: " << sDiagMain << endl;
    cout << "Somatório dos elementos abaixo da Diagonal: " << sDiagDown << endl;
    cout << "Somatório dos elementos acima da Diagonal: " << sDiagUp << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
};
