---
title: "Prova 01"
output: pdf_document
---


## Questão 1

Deseja-se construir um algoritmo que calcule as raízes e ponto de máximo de uma equação de segundo grau: $`f(x) = ax^2 + bx + c`$

É fundamental que o código consiga calcular os casos particulares, onde $` a = 0 `$ ou $` b = 0 `$ ou $` c = 0 `$, ou a combinação entre dois desses termos.
Além disso, o código deve ser capaz de calcular as raízes para os casos onde os coeficientes são números decimais.

Por fim, o algoritmo também deve apresentar se a função é crescente ou decrescente.

**Entradas**: a,b e c;
**Saídas**: x1, x2, Pm (ponto máximo) e classificação da função: *crescente* ou *decrescente*.


> **Dica**: utilize a estrutura condicional if-else para estabelecer qual a particularidade da
equação em questão.


### Resolução

#### Fluxograma

![Fluxograma do algoritmo da questão 1.](images/fluxograma_prova1_questao1.png)


#### Código-fonte

```cpp
/**
 *      Prova 1 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que calcule as raízes e ponto máximo de uma equação de segundo grau.
 *
 */
 #include <iostream>
 using namespace std;
 #include <math.h>

void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 01 - Questão 01" << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Calcular as raízes e ponto máximo de uma equação do segundo grau    ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    float coefA = 0.0f, coefB = 0.0f, coefC = 0.0f, Delta = 0.0f;
    float rootX1 = 0.0f, rootX2 = 0.0f, Yv = 0.0f, Xv = 0.0f;
    unsigned short isDecreasing = 0, isComplete = 0, hasRoot = 0;

    //
    printCabecalho();

    do {
        cout << "Informe o valor do coeficiente A: " << endl;
        cin >> coefA;
        if (coefA == 0.0f)
            cout << "Para que a equaão seja de grau 2 é imprescindível que o coeficiente A seja diferente de 0." << endl;
    } while ( coefA == 0.0f );

    cout << "Informe o valor do coeficiente B: " << endl;
    cin >> coefB;

    cout << "Informe o valor do coeficiente C: " << endl;
    cin >> coefC;

    if ((coefB != 0) && (coefC != 0))
        isComplete = 1;

    if (coefA > 0)
        isDecreasing = 1;

    Delta = pow(-(coefB), 2) - (4*coefA*coefC);
    
    if (Delta >= 0) { 
        hasRoot = 1;
        rootX1 = (float)( (-(coefB)) - sqrt(Delta) ) / (float)(2 * coefA);
        rootX2 = (float)( (-(coefB)) + sqrt(Delta) ) / (float)(2 * coefA);
    } 

    // Calcula o ponto máximo
    Xv = (float)(-(coefB)) / (float)(2 * coefA);
    Yv = (float)(-(Delta)) / (float)(4 * coefA);

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "===================================================================================" << endl;
    if (hasRoot) {
        cout << "Raiz 1 = " << rootX1 << endl;
        cout << "Raiz 2 = " << rootX2 << endl;
    } else {
        cout << "A equação não tem Raízes, pois o Delta é menor que 0." << endl;
    }
    cout << "O ponto máximo (vértice) da função é (Xv, Yv) = (" << Xv << ", " << Yv << ")" << endl;
    cout << "A função é " << ((isDecreasing==1)?"decrescente.":"crescente.") << endl;
    cout << endl;
    if (isComplete == 1) {
        cout << "Observação: A equação está COMPLETA, pois os coeficientes são diferentes de 0." << endl;
    } else {
        cout << "Observação: A equação está INCOMPLETA, pois um ou os dois coeficientes, B e C, são iguais a 0." << endl;
    }
    cout << "-----------------------------------------------------------------------------------" << endl;

};

```

------

<br/><br/><br/><br/>


## Questão 2

Desenvolva um programa que a partir do raio R de uma circunferência, estabeleça todas as medidas a seguir:

a) Diâmetro;
a) Perímetro da circunferência;
a) Área da circunferência;
a) Volume de uma esfera de mesmo raio;
a) Área da casca de uma esfera de mesmo raio.


**Entrada**: R (raio).
**Saídas**: D(diâmetro), Pc(perímetro da circunferência), Ac(área da circunferência), Ve(volume de uma esfera), Ae(área da casca de uma esfera).


### Resolução

#### Fluxograma

![Fluxograma do algoritmo da questão 2.](images/fluxograma_prova1_questao2.png)

#### Código-fonte

```cpp
/**
 *      Prova 1 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que calcule as raízes e ponto máximo de uma equação de segundo grau.
 *
 */
 #include <iostream>
 using namespace std;
 #include <math.h>

void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 01 - Questão 02" << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Calcular medidas de uma circunferência/esfera através do raio    ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    float raio = 0.0f, diametro = 0.0f, perimetro = 0.0f, areaCircunf = 0.0f;
    float volEsfera = 0.0f, areaCascaEsfera = 0.0f;

    //
    printCabecalho();

    do {
        cout << "Informe o valor do Raio de uma circunferência (Cm): " << endl;
        cin >> raio;
        if (raio <= 0.0f)
            cout << "É necessário que o Raio seja maior que 0." << endl;
    } while ( raio <= 0.0f );

    // Calcula Diâmetro
    diametro = 2 * raio;

    // Calcula Perímetro
    perimetro = 2 * M_PI * raio;

    // Calcula a Área da Circunferência
    areaCircunf = M_PI * pow(raio, 2);

    // Calcula Volume de uma Esfera
    volEsfera = (float)(4 * M_PI * pow(raio, 3)) / (float)(3);

    // Calcula a Área da Casca de uma Esfera
    areaCascaEsfera = 4 * M_PI * pow(raio, 2);

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "===================================================================================" << endl;
    cout << "a) Diâmetro = " << diametro << " Cm "<< endl;
    cout << "b) Perímetro da circunferência = " << perimetro << " Cm "<< endl;
    cout << "c) Área da circunferência = " << areaCircunf << " Cm quadrados"<< endl;
    cout << "d) Volume de uma esfera = " << volEsfera  << " Cm cúbicos"<< endl;
    cout << "e) Área da casca de uma esfera = " << areaCascaEsfera << " Cm quadrados"<< endl;
    cout << "-----------------------------------------------------------------------------------" << endl;

};

```

------

<br/><br/><br/><br/>


## Questão 3

Desenvolva um algoritmo que seja capaz de determinar o fatorial de um número inteiro N. (***O valor de N deve ser no máximo 12***)

**Entrada**: N.
**Saída**: F.

> **Dica**: utilize a estrutura de repetição while.


### Resolução

#### Fluxograma

![Fluxograma do algoritmo da questão 3.](images/fluxograma_prova1_questao3.png)



#### Código-fonte

```cpp
/**
 *      Prova 1 - Questão 03
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que recebe 1 número e calcula o seu fatorial
 *
 */
 #include <iostream>
 using namespace std;

void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Prova 01 - Questão 03" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Calcular o fatorial de um número                         ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    int number = 0, somatorio = 1;

    //
    printCabecalho();

    do {
        if (number != 0) {
            cout << endl;
            cout << endl;
            cout << "O número deve estar entre 1 e 12. " << endl;
        }
        cout << "Informe um número inteiro entre 1 e 12: " << endl;
        cin >> number;
    } while ( !((number > 0) && (number <= 12)) );

    for (int idx = number; idx > 0; idx--) {
        //cout << "<DEBUG> ["<<idx<<"]" << somatorio << " * " << idx << endl;
        somatorio *= idx;
    }


    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "========================================================================" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << "Fatorial de " << number << ", é: " << somatorio << endl;

};


```

------

<br/><br/><br/><br/>


## Questão 4

Desenvolva um algoritmo que pegue 4 números inteiros escritos em qualquer ordem e apresentem eles em ordem crescente.


**Entrada**: N1, N2, N3 e N4.
**Saída**: N1, N2, N3 e N4 (***ordem crescente***).


> **Dica**: utilize a estrutura de repetição for.

### Resolução

Para resolver essa questão eu implementei uma lista duplamente encadeada de forma que o processo de inserção do número já seja uma inserção ordenada. Com isso, após a leitura dos números será feita apenas a exibição, de forma simples, percorrendo a lista do primeiro ao último.

> **Observação**: Durante a leitura de cada número a aplicação irá exibir os números que estão na fila de forma ordenada, comprovando que a ordenação está ocorrendo durante a inserção.

> **Dica**: Utilize as sequências de números: [10,5,20,15], [10,5,8,20], [20,15,10,8] e/ou [10,50,40,200]

#### Fluxograma

![Fluxograma do algoritmo da questão 4.](images/fluxograma_prova1_questao4.png)


#### Código-fonte

```cpp
/**
 *      Prova 1 - Questão 04
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que recebe 4 números e os exibe em ordem crescente
 *
 */
 #include <iostream>
 using namespace std;

class Item {
    private:
        int vlr;
        Item* anterior;
        Item* proximo;
    public:
        Item(int valor)
        {
            this->vlr = valor;
            this->anterior = NULL;
            this->proximo = NULL;
        };

        Item* getProximo()
        {
            return proximo;
        };

        void setProximo(Item* proximo)
        {
            this->proximo = proximo;
        };

        Item* getAnterior()
        {
            return anterior;
        };

        void setAnterior(Item* anterior)
        {
            this->anterior = anterior;
        };

        int getValor()
        {
            return vlr;
        };
};

class Lista {
    private:
        int quantidade;
        Item* primeiro;
        Item* ultimo;
    public:
        Lista()
        {
            this->quantidade = 0;
            this->primeiro = NULL;
            this->ultimo = NULL;
        };

        virtual ~Lista()
        {
            delete primeiro;
        };

        int insereOrdenado(int valor)
        {
            Item* item = new Item(valor);
            if (this->primeiro == NULL) { 
                this->quantidade = 1;
                this->primeiro = item;
            } else {
                unsigned short inserted = 0;
                Item* aux = this->primeiro;
                // Verifica se é menor que o primeiro
                if (item->getValor() < aux->getValor()) {
                    item->setAnterior(NULL);
                    item->setProximo(this->primeiro);
                    this->primeiro->setAnterior(item);
                    this->primeiro = item;
                } else {
                    while (aux->getProximo() != NULL && !inserted) {
                        if (item->getValor() <= aux->getValor()) {
                            inserted = 1;
                            item->setProximo(aux);
                            item->setAnterior(aux->getAnterior());
                            aux->getAnterior()->setProximo(item);
                            aux->setAnterior(item);
                        }
                        aux = aux->getProximo();
                    }

                    if (inserted == 0) {
                        // Insere no final
                        if (item->getValor() < aux->getValor()) {
                            item->setProximo(aux);
                            item->setAnterior(aux->getAnterior());
                            item->getAnterior()->setProximo(item);
                            aux->setAnterior(item);
                        } else {
                            item->setAnterior(aux);
                            aux->setProximo(item);
                            this->ultimo = item;
                        }
                    }
                }
            }
        };

        void ExibeValorItensCrescente()
        {
            Item* aux = this->primeiro;
            int idx = 1;
            cout << endl;
            cout << endl;
            cout << "========================================================================" << endl;
            cout << "====    Estado da Lista    =============================================" << endl;
            cout << "========================================================================" << endl;
            while (aux->getProximo() != NULL) {
                cout << idx << " - Valor: " << aux->getValor() << endl;
                aux = aux->getProximo();
                idx++;
            }
            cout << idx << " - Valor: " << aux->getValor() << endl;
            cout << "========================================================================" << endl;
            cout << endl;
            cout << endl;
            cout << endl;
        };
};


void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Prova 01 - Questão 04" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Ordenar Números de forma crescente                       ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    Lista lista = Lista();
    int qty=0, number=0;

    //
    printCabecalho();

    for (int idx = 0; idx < 4; idx++) {
        cout << endl;
        cout << "------------------------------------------------------------------------" << endl;
        cout << "Informe o número na posição " << (idx+1) << ": " << endl;
        cin >> number;
        lista.insereOrdenado(number);
        cout << "------------------------------------------------------------------------" << endl;
        lista.ExibeValorItensCrescente();
    }


    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "========================================================================" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    lista.ExibeValorItensCrescente();

};

```

------

