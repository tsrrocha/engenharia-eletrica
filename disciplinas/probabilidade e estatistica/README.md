# **Probabilidade e Estatística**

[[_TOC_]]

## Conteúdo


### Medidas Descritivas Numéricas

[Página de conteúdo](https://gitlab.com/tsrrocha/engenharia-eletrica/-/blob/master/disciplinas/probabilidade%20e%20estatistica/medidas_descritivas_numericas.md)

### Medidas Dispersão

[Página de conteúdo](https://gitlab.com/tsrrocha/engenharia-eletrica/-/blob/master/disciplinas/probabilidade%20e%20estatistica/medidas_dispersao.md)

-----


## Atividades

### Gráfico

```mermaid
graph LR;
  ATV[Atividades]---ATV1[A1];
  ATV---ATV2[A2];
  ATV1---ATVA1_01[Questionário - 20/03/2021];
  ATV1---ATVA1_02[Exercício Semana 1 - 20/03/2021];
  ATV2---ATVA2_01[Prova - 26/03/2021];
  ATV2---ATVA2_02[Exercício Semana 2 - 27/03/2021];
```

### Links

[A1 - Questionário](https://1drv.ms/b/s!AluFhbT0LqwNh5x1AI7OAqJof9ySfQ?e=jovYun)

[A1 - Exercício Semana 1](https://1drv.ms/b/s!AluFhbT0LqwNh50Md55JhGaLrdtMTw?e=vy8yvE)

[A2 - Exercício Semana 2](https://1drv.ms/b/s!AluFhbT0LqwNh55AApfmo8bw5d8keg?e=1xxFcC)
