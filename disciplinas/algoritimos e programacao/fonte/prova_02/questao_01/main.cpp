/**
 *      Prova 2 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   21/04/2021
 *
 */
 #include <iostream>
 using namespace std;
 #include <math.h>
 
void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 02 - Questão 01                                               ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "====                                                                           ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}

int main (void)
{
    float numberElements = 0.0f;
    int numberElementsInt = 0;
    int row = 0;
    float value = 0.0f, somaGeral = 0.0f, mediaGeral = 0.0f, somaMed = 0.0f;
    float desvioPadrao = 0.0f;

    //, sDiagMain = 0.0f, sDiagDown = 0.0f, sDiagUp = 0.0f;

    //
    printCabecalho();

    do {
        cout << "Informe a quantidade de linhas que a matriz quadrada terá:" << endl;
        cin >> numberElements;
        numberElementsInt = (int) numberElements;
        if ( (numberElementsInt < 0) || ( (numberElements - numberElementsInt) != 0) ) {
            cout << "O número informado é inválido. Tem de ser um número inteiro maior que 0(zero)." << endl << endl << endl;
        }
    } while ( (numberElementsInt < 0) || ( (numberElements - numberElementsInt) != 0) );
    float vetor[numberElementsInt];

    //
    cout << "Você irá informar os valores para preencher um vetor "<< numberElementsInt <<"."<< endl;

    for (row = 0; row < numberElementsInt; row++) {
        cout << "Informe um número para a posição " << (row + 1) << ". " << endl;
        cin >> value;
        //
        vetor[row] = value;
        somaGeral += value;
    }

    // Calcula a média
    mediaGeral = (float) (somaGeral / (float) numberElementsInt);

    for (row = 0; row < numberElementsInt; row++) {
        //
        vetor[row] = pow((vetor[row] - mediaGeral), 2) ;
        somaMed += vetor[row];
    }

    // Raiz
    desvioPadrao = sqrt( (float) ( somaMed / (float) numberElementsInt ) );

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final                                                     ====" << endl;
    cout << "===================================================================================" << endl;
    cout << "Desvio padrão é: " << desvioPadrao << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
};
