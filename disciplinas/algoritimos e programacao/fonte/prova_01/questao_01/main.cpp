/**
 *      Prova 1 - Questão 01
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que calcule as raízes e ponto máximo de uma equação de segundo grau.
 *
 */
 #include <iostream>
 using namespace std;
 #include <math.h>

void printCabecalho(void)
{
    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Prova 01 - Questão 01" << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Calcular as raízes e ponto máximo de uma equação do segundo grau    ====" << endl;
    cout << "-----------------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    float coefA = 0.0f, coefB = 0.0f, coefC = 0.0f, Delta = 0.0f;
    float rootX1 = 0.0f, rootX2 = 0.0f, Yv = 0.0f, Xv = 0.0f;
    unsigned short isDecreasing = 0, isComplete = 0, hasRoot = 0;

    //
    printCabecalho();

    do {
        cout << "Informe o valor do coeficiente A: " << endl;
        cin >> coefA;
        if (coefA == 0.0f)
            cout << "Para que a equação seja de grau 2 é imprescindível que o coeficiente A seja diferente de 0." << endl;
    } while ( coefA == 0.0f );

    cout << "Informe o valor do coeficiente B: " << endl;
    cin >> coefB;

    cout << "Informe o valor do coeficiente C: " << endl;
    cin >> coefC;

    if ((coefB != 0) && (coefC != 0))
        isComplete = 1;

    if (coefA > 0)
        isDecreasing = 1;

    Delta = pow(-(coefB), 2) - (4*coefA*coefC);
    
    if (Delta >= 0) { 
        hasRoot = 1;
        rootX1 = (float)( (-(coefB)) - sqrt(Delta) ) / (float)(2 * coefA);
        rootX2 = (float)( (-(coefB)) + sqrt(Delta) ) / (float)(2 * coefA);
    } 

    // Calcula o ponto máximo
    Xv = (float)(-(coefB)) / (float)(2 * coefA);
    Yv = (float)(-(Delta)) / (float)(4 * coefA);

    cout << endl;
    cout << "===================================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "===================================================================================" << endl;
    if (hasRoot) {
        cout << "Raiz 1 = " << rootX1 << endl;
        cout << "Raiz 2 = " << rootX2 << endl;
    } else {
        cout << "A equação não tem Raízes, pois o Delta é menor que 0." << endl;
    }
    cout << "O ponto máximo (vértice) da função é (Xv, Yv) = (" << Xv << ", " << Yv << ")" << endl;
    cout << "A função é " << ((isDecreasing==1)?"decrescente.":"crescente.") << endl;
    cout << endl;
    if (isComplete == 1) {
        cout << "Observação: A equação está COMPLETA, pois os coeficientes são diferentes de 0." << endl;
    } else {
        cout << "Observação: A equação está INCOMPLETA, pois um ou os dois coeficientes, B e C, são iguais a 0." << endl;
    }
    cout << "-----------------------------------------------------------------------------------" << endl;

};
