/**
 *      Prova 1 - Questão 04
 *  @author Tiago Sousa Rocha
 *  @date   29/03/2021
 *  @brief Algoritmo que recebe 4 números e os exibe em ordem crescente
 *
 */
 #include <iostream>
 using namespace std;

class Item {
    private:
        int vlr;
        Item* anterior;
        Item* proximo;
    public:
        Item(int valor)
        {
            this->vlr = valor;
            this->anterior = NULL;
            this->proximo = NULL;
        };

        Item* getProximo()
        {
            return proximo;
        };

        void setProximo(Item* proximo)
        {
            this->proximo = proximo;
        };

        Item* getAnterior()
        {
            return anterior;
        };

        void setAnterior(Item* anterior)
        {
            this->anterior = anterior;
        };

        int getValor()
        {
            return vlr;
        };
};

class Lista {
    private:
        int quantidade;
        Item* primeiro;
        Item* ultimo;
    public:
        Lista()
        {
            this->quantidade = 0;
            this->primeiro = NULL;
            this->ultimo = NULL;
        };

        virtual ~Lista()
        {
            delete primeiro;
        };

        int insereOrdenado(int valor)
        {
            Item* item = new Item(valor);
            if (this->primeiro == NULL) { 
                this->quantidade = 1;
                this->primeiro = item;
            } else {
                unsigned short inserted = 0;
                Item* aux = this->primeiro;
                // Verifica se é menor que o primeiro
                if (item->getValor() < aux->getValor()) {
                    item->setAnterior(NULL);
                    item->setProximo(this->primeiro);
                    this->primeiro->setAnterior(item);
                    this->primeiro = item;
                } else {
                    while (aux->getProximo() != NULL && !inserted) {
                        if (item->getValor() <= aux->getValor()) {
                            inserted = 1;
                            item->setProximo(aux);
                            item->setAnterior(aux->getAnterior());
                            aux->getAnterior()->setProximo(item);
                            aux->setAnterior(item);
                        }
                        aux = aux->getProximo();
                    }

                    if (inserted == 0) {
                        // Insere no final
                        if (item->getValor() < aux->getValor()) {
                            item->setProximo(aux);
                            item->setAnterior(aux->getAnterior());
                            item->getAnterior()->setProximo(item);
                            aux->setAnterior(item);
                        } else {
                            item->setAnterior(aux);
                            aux->setProximo(item);
                            this->ultimo = item;
                        }
                    }
                }
            }
        };

        void ExibeValorItensCrescente()
        {
            Item* aux = this->primeiro;
            int idx = 1;
            cout << endl;
            cout << endl;
            cout << "========================================================================" << endl;
            cout << "====    Estado da Lista    =============================================" << endl;
            cout << "========================================================================" << endl;
            while (aux->getProximo() != NULL) {
                cout << idx << " - Valor: " << aux->getValor() << endl;
                aux = aux->getProximo();
                idx++;
            }
            cout << idx << " - Valor: " << aux->getValor() << endl;
            cout << "========================================================================" << endl;
            cout << endl;
            cout << endl;
            cout << endl;
        };
};


void printCabecalho(void)
{
    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Prova 01 - Questão 04" << endl;
    cout << "========================================================================" << endl;
    cout << "====       Ordenar Números de forma crescente                       ====" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    cout << endl;
}


int main (void)
{
    Lista lista = Lista();
    int qty=0, number=0;

    //
    printCabecalho();

    for (int idx = 0; idx < 4; idx++) {
        cout << endl;
        cout << "------------------------------------------------------------------------" << endl;
        cout << "Informe o número na posição " << (idx+1) << ": " << endl;
        cin >> number;
        lista.insereOrdenado(number);
        cout << "------------------------------------------------------------------------" << endl;
        lista.ExibeValorItensCrescente();
    }


    cout << endl;
    cout << "========================================================================" << endl;
    cout << "====       Resultado final " << endl;
    cout << "========================================================================" << endl;
    cout << "------------------------------------------------------------------------" << endl;
    lista.ExibeValorItensCrescente();

};
